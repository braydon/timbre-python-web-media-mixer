# -*- coding: utf-8 -*-

import os
from os.path import exists, getmtime
from functools import partial
from os.path import getmtime, exists
from uuid import NAMESPACE_OID, uuid5, uuid1
from datetime import datetime, time
from copy import copy
import codecs
import re
from math import floor

from jinja2 import Environment, PackageLoader
from werkzeug import Request, ClosingIterator
from werkzeug.exceptions import HTTPException

from timbre.utils import local, local_manager, url_map, Scheduler, daily_at
from timbre import views
from timbre.models import AudioPlaylist, AudioDirectory
from timbre.database import DocumentDB

BASE_PATH = "/var/local/listen"
BASE_URL = "http://sample.com"

templates = Environment(loader=PackageLoader('templates','./'))

def sec_to_min(x):
    x = int(floor(float(x)))
    sec = str(x%60)
    if len(sec) < 2: sec = "0"+sec
    return str(x/60)+":"+sec
    
templates.filters['sec_to_min'] = sec_to_min
    
db = DocumentDB(
    doc_types = {
    'AudioPlaylist':AudioPlaylist,
    'AudioDirectory':AudioDirectory},
    doc_path = BASE_PATH + "/documents"
    )

class Timbre():

    def __init__(self):
        local.application = self
        self.cron()
        self.scheduler = Scheduler()
        self.scheduler.schedule("index", time(), daily_at(time()), self.cron())
        self.dirs = db.query(doc_type="AudioDirectory")

    def __call__(self, environ, start_response):
        local.application = self
        request = Request(environ)
        local.url_adapter = adapter = url_map.bind_to_environ(environ)
        try:
            endpoint, values = adapter.match()
            handler = getattr(views, endpoint)
            response = handler(request, **values)
        except HTTPException, e:
            response = e
        return ClosingIterator(response(environ, start_response),
                               [session.remove, local_manager.cleanup])

    def cron(self):
        audio = [AudioDirectory("Mp3Blog", BASE_PATH + "/static/music/mp3", BASE_URL + "/static/music/mp3"),
                 AudioDirectory("Podcast", BASE_PATH + "/static/music/podcast", BASE_URL + "/static/music/podcast")]
        map(lambda x: x.cron(), audio)
        map(lambda x: db.save(x), audio)
