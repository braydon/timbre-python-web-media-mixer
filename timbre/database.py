import gdbm
import simplejson as json

class NotFound(Exception):
    pass

class Document(object):
    """A basic document object class."""
    def __init__(self, **kwargs):
        self.doc_type = "Document"
        self._use(kwargs)
        if not hasattr(self, "uuid"):
            self.uuid = uuid1().hex

    def _use(self, kwargs):
        if isinstance(kwargs, dict):
            for key, value in kwargs.items(): self.__setattr__(key, value)
        else: raise

    def _attributes(self):
        doc_dict = self.__dict__
        for key, value in doc_dict.items():
            if key[0] == "_":
                del doc_dict[key]
        return doc_dict

    def _attempt(self, args, index):
        try: return args[index]
        except: return False

    def _setter(self, attr, args, index, default):
        if not hasattr(self, str(attr)):
            value = self._attempt(args, index) or default
            self.__setattr__(attr, value)

class DocumentDB(object):
    """
    This is a collection of functions to be used to create
    view-indexes, as-well as query results from the database.
    """

    def __init__(self, doc_types, doc_path):
        """
        doc_types = {'Note':Note(),'Event':Event()}
        doc_path = /var/local/documents/
        """
        self.doc_types = doc_types
        self.doc_path = doc_path
        self.index_path = self.doc_path + "/index"
        self.views = self._views()

    def _views(self):
        return set()

    def _open(self, key=False):
        if not key:
            if not exists(self.index_path+".gdbm"):
                return gdbm.open(self.index_path+".gdbm", "nf")
            return gdbm.open(self.index_path+".gdbm", "wf")
        v_path = self.index_path+"_"+key+".gdbm"
        if not exists(v_path) or getmtime(self.index_path+".gdbm") > getmtime(v_path):
            self._save_view(key)
        return gdbm.open(v_path,"wf")

    def _save_view(self, by_key):
        """
        To be used to build a sub index based on a key.
        """
        self.views.add(by_key)
        index = self._open()
        view = gdbm.open(self.index_path+"_"+by_key+".gdbm",'nf')
        def invert(key):
            value = json.loads(index[key])
            if value.has_key(by_key):
                new_key, uuid = value[by_key], value["uuid"]
                try:
                    res = json.loads(view[new_key])
                    res.append(uuid)
                    view[new_key] = json.dumps(res)
                except (KeyError, AttributeError):
                    view[new_key] = json.dumps([uuid])
        key = index.firstkey()
        while key != None:
            invert(key)
            key = index.nextkey(key)
        view.sync()

    def query(self, *uuids, **attributes):
        def into_object(value):
            kwargs = {}
            for k,v in json.loads(value).items():
                kwargs[str(k)] = str(v)
            doc = self.doc_types[kwargs["doc_type"]](**kwargs)
            return copy(doc)
        index = self._open()
        try:
            if attributes and len(attributes) == 1:
                #todo: AND, OR, NOT, SIMILAR, LESS_THAN, and GREATER_THAN
                view = self._open(attributes.keys()[0])
                value = attributes.values()[0]
                return [into_object(index[uuid]) for uuid in json.loads(view[value])]
            elif uuids:
                return [into_object(index[uuid]) for uuid in uuids]
            else:
                return [into_object(index[index.firstkey()])]
        except KeyError:
            raise NotFound

    def save(self, docs):
        #todo: also update views...
        index = self._open()
        def _update(doc):
            if any(isinstance(doc, v) for k, v in self.doc_types.items()):
                index[doc.uuid] = json.dumps(doc._attributes())
                #todo: open views, and invert item
            else:
                index.sync()
                raise Exception("%s is not one of %s" %
                                (doc, " | ".join([str(dt) for dt in self.doc_types])))
        if isinstance(docs, list): map(_update, docs)
        else: _update(docs)
        index.sync()
