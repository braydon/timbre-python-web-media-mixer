from timbre.utils import expose

@expose("/admin")
def admin(request):
    pass

@expose("/<uuid>")
def editor(request, uuid=None):
    url_suffix = "?"+"&".join([k+"="+v for k,v in kwargs.items()])
    site_tmpl = templates.get_template("editor.html")
    page = ""
    directories = []
    playlist = None
    audio_files = None
    if uuid:
        try: 
            playlist = db.query(uuid)[0]
        except NotFound: 
            raise cherrypy.HTTPError(404)

    def date_cmp(x,y):
        a = x["modified"]
        b = y["modified"]
        if a < b: return 1
        elif x == y: return 0
        else: return -1

    for d in self.dirs:
        if kwargs.has_key("search") and len(kwargs["search"]) >= 1:
            audio = d.query(kwargs["search"])
        else: audio = json.loads(d.audio)
        if playlist:
            def mark_current(x):
                if x["file"] in playlist.audio_files: x["current"] = True
                else: x["current"] = False
                return x            
            map(mark_current, audio)
        audio = sorted(audio, date_cmp)
        directories.append((d,audio))
    if playlist:
        page = playlist._html
        audio_files = playlist.audio_files            
    else: page = None
    site_html = site_tmpl.render({"playlist_html":page,
                             "directories":directories,
                             "uuid":uuid,
                             "playlist":audio_files,
                             "kwargs":kwargs,
                             "url_suffix":url_suffix,
                             "kwargs":kwargs})
    base_tmpl = templates.get_template("base.html")
    return base_tmpl.render({"body":site_html})

@expose("/add/<uuid>/<directory>/<file>")
def add(request, uuid, file, directory):
    try: 
        playlist = db.query(uuid)[0]
    except NotFound: 
        raise cherrypy.HTTPError(404)
    audio_files = json.loads(playlist.audio_files)
    audio_files.insert(0, json.loads([file, directory]))
    playlist = AudioPlaylist(json.dumps(audio_files))
    try: 
        db.query(playlist.uuid)
    except NotFound: 
        db.save(playlist)
    url = BASE_URL+"/editor/"+playlist.uuid+url_suffix
    raise cherrypy.HTTPRedirect(url)

@expose("/new/<directory>/<file>")
def new(request):
    playlist = AudioPlaylist(json.dumps([file,directory]]))
    try: db.query(playlist.uuid)
    except NotFound: db.save(playlist)
    url = BASE_URL+"/editor/"+playlist.uuid+url_suffix
    raise cherrypy.HTTPRedirect(url)

@expose("/subtract/<uuid>/<directory>/<file>")
def subtract(request):
    try: 
        playlist = db.query(uuid)[0]
    except NotFound: 
        raise cherrypy.HTTPError(404)
    audio_file = [file, directory]
    audio_files = json.loads(playlist.audio_files)
    for c,x in enumerate(audio_files):
        if x == audio_file: del audio_files[c]
    playlist = AudioPlaylist(json.dumps(audio_files))
    try: 
        db.query(playlist.uuid)
    except NotFound: 
        db.save(playlist)
    url = BASE_URL+"/editor/"+playlist.uuid+url_suffix
    raise cherrypy.HTTPRedirect(url)

@expose("/playlist/<uuid>")
def playlist(request):
    uuid = args[0]
    try: playlist = db.query(uuid)[0]
    except NotFound: raise cherrypy.HTTPError(404)
    if len(args) == 2:
        if args[1] == "js":
            return playlist._javascript
        elif args[1] == "html":
            return playlist._html
    return playlist._page


