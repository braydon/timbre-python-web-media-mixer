import kaa.metadata
from ming import *
import simplejson as json
import xapian

from timbre.database import Document
from timbre.application import templates

class AudioPlaylist(Document):
    def __init__(self, *args, **kwargs):
        super(AudioPlaylist, self).__init__(**kwargs)
        attrs = [("audio_files", None)]
        for c, a in enumerate(attrs):
            self._setter(a[0], args, c, a[1])
        self.doc_type = "AudioPlaylist"
        self.uuid = uuid5(NAMESPACE_OID, self.audio_files).hex
        if not hasattr(self, "info"):
            self.info = json.dumps(self._get_info())
        self._swf_streamer = self._swf_streamer("/static/streamer.swf")
        self._javascript = self.javascript()
        self._html = self.html()
        self._page = self.page()

    def html(self):
        t = templates.get_template("playlist.html")
        return t.render({'BASE_URL':BASE_URL, 'id':self.uuid, 'info':json.loads(self.info)})

    def page(self):
        return templates.get_template("base.html").render({"body":self._html})

    def javascript(self):
        t = templates.get_template("playlist.js")
        return t.render({'info':self.info,'swf_streamer':self._swf_streamer,'id':self.uuid})

    def _get_info(self):
        """
        Extracts meta-data from the audio files and adds some extra needed
        info such as the embed code to be used to play the audio stream.
        """
        metadata = []
        directories = db.query(doc_type="AudioDirectory")
        def abs_paths(x):
            for d in directories:
                if x[1] == d.title:
                    return (d.audio_path + "/" + x[0], d.audio_url + "/" + x[0])

        files = map(abs_paths, json.loads(self.audio_files))
        for f in files:
            info_raw = kaa.metadata.parse(f[0])
            info = {}
            for attr in dir(info_raw):
                value = info_raw.__getattribute__(attr)
                t = type(value)
                if not attr[0] == "_" and not attr == "thumbnail":
                    if t == int or t == float:
                        info[attr] = value
                    elif t == str or t == unicode:
                        info[attr] = unicode(value)
            info["url"] = unicode(f[1])
            if info.has_key("length"): metadata.append(info)
            else: raise Exception()
        return metadata

    def _swf_streamer(self, output):
        if exists(BASE_PATH+output):
            return output
        Ming_useSWFVersion(7)
        Ming_setScale(20)
        swf = SWFMovie()
        swf.setRate(12)
        swf.setNumberOfFrames(1)
        swf.setDimension(1,1)
        str_action = """
        var sound = new Sound();
        function send_position(){
            getURL("javascript:db_"+_level0.playlist+"_loop("+_level0.track+","+sound.position+")");
        }
        if (_level0.sound_file != undefined) {
            sound.loadSound(_level0.sound_file, true);
            sound.start();
            setInterval(send_position, 1000);
        }""" % {"id":self.uuid}
        swf.add(SWFAction(str_action))
        swf.save(output)
        return BASE_URL+output


class AudioDirectory(Document):

    def __init__(self, *args, **kwargs):
        super(AudioDirectory, self).__init__(**kwargs)
        attrs = [("title", None),
                 ("audio_path", None),
                 ("audio_url", None),
                 ("_exts", [".mp3",".ogg"])]

        for c, a in enumerate(attrs):
            self._setter(a[0], args, c, a[1])

        self.doc_type = "AudioDirectory"
        if hasattr(self, "audio_path") and self.audio_path:
            self.uuid = uuid5(NAMESPACE_OID, self.audio_path).hex
            if not hasattr(self, "audio"):
                self.audio = json.dumps(self.get_audio())
        self._dbpath = BASE_PATH+"/documents/search_"+self.doc_type
        #self._terms = [("artist","A"),("title","S"),("album","M"),("genre","G")]
        self._values = ["file","url","audio_title","audio_path","audio_url","artist","title","album","genre","length","modified","date"]

    def cron(self):
        self.audio = json.dumps(self.get_audio())
        self.index()

    def __iter__(self):
        return iter(self.audio)

    def get_audio(self):
        """
        Looks for files in the audio_path directory that match the
        exts (extensions) defined. It then uses kaa to get
        the metadata for each file and returns a list with dictionaries
        of all the files metadata.
        """
        def keep_extensions(path, keywords):
            path = path.lower()
            if any(path.find(value) >= 0 for value in keywords): return True
            else: return False

        def get_metadata(file):
            info_raw = kaa.metadata.parse(self.audio_path +"/"+ file)
            info = {}
            for attr in dir(info_raw):
                value = info_raw.__getattribute__(attr)
                t = type(value)
                if not attr[0] == "_" and not attr == "thumbnail":
                    if t == int or t == float:
                        info[attr] = value
                    elif t == str or t == unicode:
                        info[attr] = unicode(value)
            info["url"] = unicode(self.audio_url +"/"+ file)
            info["file"] = unicode(file)
            info["audio_title"] = unicode(self.title)
            info["audio_path"] = unicode(self.audio_path)
            info["audio_url"] = unicode(self.audio_url)
            info["modified"] = getmtime(self.audio_path +"/"+ file)
            info["date"] = datetime.fromtimestamp(info["modified"]).strftime("%a, %d %b %Y")
            if not info.has_key("length"):
                raise Exception
            return info

        files = []
        for t in os.walk(self.audio_path):
            for f in t[2]:
                files.append(f)
        files = map(get_metadata, filter(partial(keep_extensions, keywords=self._exts), files))
        return files


    def index(self):
        database = xapian.WritableDatabase(self._dbpath, xapian.DB_CREATE_OR_OPEN)
        indexer = xapian.TermGenerator()
        stemmer = xapian.Stem("english")
        indexer.set_stemmer(stemmer)
        used = []

        for metadata in self.get_audio():
            para = " ".join([unicode(x) for x in metadata.values()])
            doc = xapian.Document()
            doc.set_data(para.encode("utf-8"))
            indexer.set_document(doc)
            indexer.index_text(para)
            for c, value in enumerate(self._values):
                if metadata.has_key(value):
                    doc.add_value(c, unicode(metadata[value]))
            #this is such a ugly way to make a docid - collisions!!!!!!
            docid = int(str(hash(metadata["file"])).replace("-","0")[:9])
            used.append(docid)
            database.replace_document(docid, doc)
        for c,x in enumerate(used):
            for b,y in enumerate(used):
                if x == y and not c == b:
                    raise Exception

    def search(self, query, offset=0, count=1000):
        database = xapian.Database(self._dbpath)
        enquire = xapian.Enquire(database)

        query_string = query
        qp = xapian.QueryParser()
        stemmer = xapian.Stem("english")
        qp.set_stemmer(stemmer)
        qp.set_database(database)
        qp.set_stemming_strategy(xapian.QueryParser.STEM_SOME)
        query = qp.parse_query(query_string)
        enquire.set_query(query)
        matches = enquire.get_mset(0, 1000)

        results = []
        for c, m in enumerate(matches):
            results.append({})
            results[c]["rank"] = m.rank + 1
            results[c]["percent"] = m.percent
            results[c]["docid"] = m.docid
            for i, v in enumerate(self._values):
                results[c][v] = m.document.get_value(i)
        return results
