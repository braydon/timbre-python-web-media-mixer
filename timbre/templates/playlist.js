function sec_to_min(x){
sec = Math.round(x%60)
if(sec < 10) sec = "0"+sec
return Math.floor(x/60) + ":" + sec
}
function db_{{id}}_loop(track, position){
var debug = false;
var playlist, swf, title, info, extra, position_secs, length, next_track;
var data = {{info}};
if(track == undefined){track = 0;next_track = 0;}
else {if(data.length == 1){next_track = 0;} else {next_track = track + 1;
if (next_track >= data.length){next_track = 0;}}}  
var playing_length = Math.round(data[track]["length"]);
if (position){var position_secs = Math.round(position/1000);}
if (debug){
for(i=0,l=data.length;i<l;i++){console.log("data: "+data[i].toSource());}
console.log("track: "+track);
console.log("length: "+playing_length);
console.log("seconds: "+position_secs);
console.log("next: "+next_track);
}
if (position_secs == playing_length || position_secs == undefined){
var d = data[next_track];
swf = document.getElementById("dublab-swf-{{id}}");
function swf_tmpl(url){return "<object width='1' height='1'><param name='movie' value='{{swf_streamer}}'></param><param name='FlashVars' value='soundFile="+url+"&playList=%(id)s&track="+next_track+"'></param><embed src='{{swf_streamer}}' FlashVars='sound_file="+url+"&playlist={{id}}&track="+next_track+"' type='application/x-shockwave-flash' width='1' height='1'></embed></object>";
}
swf.innerHTML = swf_tmpl(d["url"]);
title = document.getElementById("dublab-title-{{id}}");
title.innerHTML = d["title"];
info = document.getElementById("dublab-info-{{id}}");
//todo: if no album, default to something else or show nothing.
info.innerHTML = "<em>by</em> <strong>"+d["artist"]+ "</strong> <br/><em>from</em> <strong>" +d["album"]+"</strong>";
extra = document.getElementById("dublab-extra-{{id}}");
extra.innerHTML = "<p>bitrate: " +d["bitrate"] +"kbps length: "+ sec_to_min(d["length"]) +" seconds </p> <p><a href='' class='db-button'>play</a><a href='' class='db-button'>stop</a><a href='' class='db-button'>&larr;</a><a href='' class='db-button'>&rarr;</a><a href='' class='db-button db-download'>download</a></p>";
}}
playlist = document.getElementById("dublab-playlist-{{id}}");
db_{{id}}_loop();